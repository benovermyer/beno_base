# frozen_string_literal: true

# InSpec test for recipe beno_base::default

# The InSpec reference, with examples and extensive documentation, can be
# found at https://www.inspec.io/docs/reference/resources/

describe user('ben') do
  it { should exist }
  its('groups') { should eq %w(ben emperor docker caddy) }
  its('shell') { should eq '/usr/bin/zsh' }
end

describe user('caddy') do
  it { should exist }
  its('groups') { should eq ['caddy'] }
  its('shell') { should eq '/usr/sbin/nologin' }
end

describe docker_container('portainer') do
  it { should exist }
  it { should be_running }
  its('ports') { should eq '8000/tcp, 0.0.0.0:9000->9000/tcp' }
end

describe port(9000) do
  it { should be_listening }
end

describe service('caddy-api') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end

describe sshd_config do
  its('PubkeyAuthentication') { should eq 'yes' }
  its('PermitRootLogin') { should eq 'no' }
  its('PasswordAuthentication') { should eq 'no' }
end

describe command('cargo --version') do
  its('stderr') { should match 'cargo' }
end
