# frozen_string_literal: true

#
# Cookbook:: beno_base
# Spec:: default
#
# Copyright:: 2021, Ben Overmyer, All Rights Reserved.

require 'spec_helper'

describe 'beno_base::default' do
  context 'When all attributes are default, on Ubuntu 20.04' do
    # for a complete list of available platforms and versions see:
    # https://github.com/chefspec/fauxhai/blob/master/PLATFORMS.md
    platform 'ubuntu', '20.04'

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end
  end
end
