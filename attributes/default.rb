# frozen_string_literal: true

#
# Cookbook:: beno_base
# Attributes:: default
#
# Copyright:: 2021, Ben Overmyer, All Rights Reserved.

default['beno_base']['ssh_keys'] = [
  'ecdsa-sha2-nistp521 AAAAE2VjZHNhLXNoYTItbmlzdHA1MjEAAAAIbmlzdHA1MjEAAACFBAC92jl21UXhAiNhfPH4yfvF4mHNnnat8oi83qGSEJHunen9v9owXxoh+hUQe6B53Q3bMwn6NT8lcbTD213YSSgR3wHhQNvVyK83R2Bz2sFeLYFZLpsJ6TGkoJsVpgC/GDCREKr3tjxuWFqC8pALN4PKG2kpdkGrqPW0HPpeMs284FtZgQ== benovermyer@Bens-MacBook-Pro.local',
]
default['beno_base']['motd'] = 'This Server Managed by Ben Overmyer'

# Caddy settings
default['beno_base']['enable_caddy'] = false
default['beno_base']['caddy_version'] = '2.3.0'

# Docker settings
default['beno_base']['enable_docker'] = false
