# frozen_string_literal: true

name 'beno_base'
maintainer 'Ben Overmyer'
maintainer_email 'ben@overmyer.net'
license 'All Rights Reserved'
description 'Configures a system with most things I need'
version '0.3.0'
chef_version '>= 16.0'

supports 'ubuntu'

depends 'docker'
depends 'line'
