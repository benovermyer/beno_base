# frozen_string_literal: true

#
# Cookbook:: beno_base
# Recipe:: caddy
#
# Copyright:: 2021, Ben Overmyer, All Rights Reserved.

caddy_version = node['beno_base']['caddy_version']

user 'caddy' do
  comment 'Caddy web server'
  home '/var/lib/caddy'
  shell '/usr/sbin/nologin'
end

group 'caddy' do
  members %w(ben caddy)
end

directory '/etc/caddy' do
  owner 'root'
  group 'caddy'
end

directory '/etc/ssl/caddy' do
  owner 'root'
  group 'caddy'
  mode '0770'
end

directory '/var/log/caddy' do
  owner 'root'
  group 'caddy'
end

remote_file "#{Chef::Config[:file_cache_path]}/caddy.tar.gz" do
  source "https://github.com/caddyserver/caddy/releases/download/v#{caddy_version}/caddy_#{caddy_version}_linux_amd64.tar.gz"
  not_if { ::File.exist?('/usr/bin/caddy') }
end

execute 'unarchive' do
  command "tar -zxvf #{Chef::Config[:file_cache_path]}/caddy.tar.gz -C /usr/bin/"
  not_if { ::File.exist?('/usr/bin/caddy') }
end

systemd_unit 'caddy-api.service' do
  content({ Unit: {
            Description: 'caddy',
            Documentation: 'https://caddyserver.com/docs/',
            After: 'network.target',
          },
            Service: {
              User: 'caddy',
              Group: 'caddy',
              ExecStart: '/usr/bin/caddy run --environ --resume',
              TimeoutStopSec: '5s',
              LimitNOFILE: 1_048_576,
              LimitNPROC: 512,
              PrivateTmp: true,
              ProtectSystem: 'full',
              AmbientCapabilities: 'CAP_NET_BIND_SERVICE',
            }, Install: {
              WantedBy: 'multi-user.target',
            } })
  verify false
  action %i(create enable start)
end
