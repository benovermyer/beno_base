# frozen_string_literal: true

#
# Cookbook:: beno_base
# Recipe:: default
#
# Copyright:: 2021, Ben Overmyer, All Rights Reserved.

package %w(apt-transport-https ca-certificates curl git htop postfix rsync software-properties-common vim zsh)

user 'ben' do
  username 'ben'
  comment 'Ben Overmyer'
  shell '/usr/bin/zsh'
  home '/home/ben'
  manage_home true
end

group 'emperor' do
  comment 'High Administrators'
  members %w(ben)
end

sudo 'emperor' do
  groups 'emperor'
  nopasswd true
end

directory '/home/ben/.ssh' do
  owner 'ben'
  group 'ben'
  mode '0700'
end

template '/home/ben/.ssh/authorized_keys' do
  source 'authorized_keys.erb'
  owner 'ben'
  group 'ben'
  mode '0600'
end

template '/etc/motd' do
  source 'motd.erb'
end

service 'ssh' do
  action :nothing
end

replace_or_add 'Enable public key authentication' do
  path '/etc/ssh/sshd_config'
  pattern '^PubkeyAuthentication*'
  line 'PubkeyAuthentication yes'
  notifies :restart, 'service[ssh]', :delayed
end

replace_or_add 'Disable root login' do
  path '/etc/ssh/sshd_config'
  pattern '^PermitRootLogin*'
  line 'PermitRootLogin no'
  notifies :restart, 'service[ssh]', :delayed
end

replace_or_add 'Disable password authentication' do
  path '/etc/ssh/sshd_config'
  pattern '^PasswordAuthentication*'
  line 'PasswordAuthentication no'
  notifies :restart, 'service[ssh]', :delayed
end

execute 'Get Rust installer' do
  command "curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | cat > /home/ben/install_rust.sh"
  user 'ben'
  not_if { ::File.exist?('/home/ben/.cargo/bin/cargo') }
end

execute 'Install Rust' do
  command "HOME=/home/ben sh /home/ben/install_rust.sh -y"
  user 'ben'
  not_if { ::File.exist?('/home/ben/.cargo/bin/cargo') }
end

execute 'apt_update' do
  command 'apt-get update'
end

package 'gcc'
package 'libxcb-render0-dev' 
package 'libxcb-shape0-dev'
package 'libxcb-xfixes0-dev'

execute 'Install Amp' do
  command "HOME=/home/ben /home/ben/.cargo/bin/cargo install amp"
  user 'ben'
  not_if { ::File.exist?('/home/ben/.cargo/bin/amp') }
end

include_recipe '::docker' if node['beno_base']['enable_docker']
include_recipe '::caddy' if node['beno_base']['enable_caddy']
