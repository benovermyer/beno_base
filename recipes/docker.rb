# frozen_string_literal: true

#
# Cookbook:: beno_base
# Recipe:: docker
#
# Copyright:: 2021, Ben Overmyer, All Rights Reserved.

group 'docker' do
  comment 'Docker Users'
  members %w(ben)
end

docker_installation 'default'

execute 'docker swarm init' do
  command 'docker swarm init'
  creates '/root/docker_swarm_initialized'
end

execute 'touch docker swarm initialized file' do
  command 'touch /root/docker_swarm_initialized'
end

docker_volume 'portainer' do
  action :create
end

docker_image 'portainer/portainer-ce' do
  tag '2.1.1'
end

docker_container 'portainer' do
  repo 'portainer/portainer-ce'
  tag '2.1.1'
  volumes %w(portainer:/data /var/run/docker.sock:/var/run/docker.sock)
  port '9000:9000'
  restart_policy 'always'
end
