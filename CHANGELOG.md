# beno_base CHANGELOG

This file is used to list changes made in each version of the `beno_base` cookbook.

## 0.3.0

- Add installation of Rust
- Add installation of the Amp editor

## 0.2.0

- Make caddy and docker optional

## 0.1.0

- Release initial version
